package lab01.example.model;

public abstract class AbstractBankAccount {
    private double balance;
    private final AccountHolder holder;

    public AbstractBankAccount(final AccountHolder holder, final double balance) {
        this.holder = holder;
        this.balance = balance;
    }

    public AccountHolder getHolder() {
        return this.holder;
    }

    public double getBalance() {
        return this.balance;
    }

    protected void changeBalanceIfPossible(final int userId, final double amount) {
        if (this.checkUser(userId) && this.isBalanceSufficient(amount)) {
            this.balance += amount;
        }
    }

    private boolean isBalanceSufficient(final double amount){
        return this.balance + amount >= 0;
    }

    private boolean checkUser(final int id) {
        return this.holder.getId() == id;
    }
}
