package lab01.example.model;

public interface BankAccountWithAtm extends BankAccount {
    void depositWithAtm(int userId, double amount);
    void withdrawWithAtm(int userId, double amount);
}
