package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm {

    private static final double ATM_FEE = 1;

    public SimpleBankAccountWithAtm(final AccountHolder holder, final double balance) {
        super(holder, balance);
    }

    @Override
    public void depositWithAtm(final int userId, final double amount) {
        this.changeBalanceIfPossible(userId, amount - ATM_FEE);
    }

    @Override
    public void withdrawWithAtm(final int userId, final double amount) {
        this.changeBalanceIfPossible(userId, -amount - ATM_FEE);
    }
}
