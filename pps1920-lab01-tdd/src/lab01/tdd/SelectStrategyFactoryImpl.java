package lab01.tdd;

public class SelectStrategyFactoryImpl implements SelectStrategyFactory {
    @Override
    public SelectStrategy multipleOf(int number) {
        return x -> x % number == 0;
    }

    @Override
    public SelectStrategy even() {
        return multipleOf(2);
    }

    @Override
    public SelectStrategy isEqualTo(int number) {
        return x -> x == number;
    }
}
