package lab01.tdd;

import java.util.*;
import java.util.stream.Stream;

public class CircularListImpl implements CircularList {

    private final List<Integer> items;
    private int position;

    public CircularListImpl() {
        this.items = new ArrayList<>();
        this.position = 0;
    }

    @Override
    public void add(int element) {
        this.items.add(element);
    }

    @Override
    public int size() {
        return this.items.size();
    }

    @Override
    public boolean isEmpty() {
        return this.items.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        final Optional<Integer> ret = this.currentElement();
        if (ret.isPresent()) {
            this.position = (this.position + 1) % this.size();
        }
        return ret;
    }

    @Override
    public Optional<Integer> previous() {
        this.position = this.position == 0
                ? this.size() - 1
                : this.position - 1;
        return this.currentElement();
    }

    @Override
    public void reset() {
        this.position = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        return Stream.generate(this::next)
                .limit(this.size())
                .map(Optional::get)
                .filter(strategy::apply)
                .findFirst();
    }

    private Optional<Integer> currentElement() {
        return isEmpty() ? Optional.empty() : Optional.of(this.items.get(this.position));
    }
}
