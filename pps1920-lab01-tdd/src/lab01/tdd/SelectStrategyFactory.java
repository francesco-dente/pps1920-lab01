package lab01.tdd;

public interface SelectStrategyFactory {
    SelectStrategy multipleOf(int number);
    SelectStrategy even();
    SelectStrategy isEqualTo(int number);
}
