import lab01.tdd.CircularListImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private CircularListImpl list;

    @BeforeEach
    void setUp() {
        this.list = new CircularListImpl();
    }

    @Test
    void startsEmpty() {
        assertTrue(list.isEmpty());
    }

    @Test
    void sizeMatchesAddedElements()
    {
        list.add(1);
        list.add(2);
        list.add(3);
        assertEquals(3, list.size());
    }

    @Test
    void nextReturnsEmptyIfListIsEmpty() {
        assertFalse(list.next().isPresent());
    }

    @Test
    void nextPointsToFirst() {
        list.add(1);
        list.add(2);
        assertEquals(1, list.next().get());
    }

    @Test
    void callingNextMultipleTimesReturnsNextItem() {
        List<Integer> items = List.of(1, 2, 3);
        items.forEach(list::add);

        List<Integer> result = Stream.generate(list::next)
                .map(Optional::get)
                .limit(3)
                .collect(Collectors.toList());

        assertEquals(items, result);
    }

    @Test
    void nextLoops() {
        List<Integer> items = List.of(1, 2, 3);
        items.forEach(list::add);

        List<Integer> result = Stream.generate(list::next)
                .map(Optional::get)
                .limit(6)
                .collect(Collectors.toList());

        assertEquals(List.of(1, 2, 3, 1, 2, 3), result);
    }

    @Test
    void previousReturnsEmptyIfListIsEmpty() {
        assertFalse(list.previous().isPresent());
    }

    @Test
    void previousPointsToLast() {
        list.add(1);
        list.add(2);
        assertEquals(2, list.previous().get());
    }

    @Test
    void callingPreviousMultipleTimesReturnsPreviousItems() {
        List<Integer> items = List.of(1, 2, 3);
        items.forEach(list::add);
        List<Integer> result = Stream.generate(list::previous)
                .map(Optional::get)
                .limit(3)
                .collect(Collectors.toList());
        assertEquals(List.of(3, 2, 1), result);
    }

    @Test
    void previousLoops() {
        List<Integer> items = List.of(1, 2, 3);
        items.forEach(list::add);

        List<Integer> result = Stream.generate(list::previous)
                .map(Optional::get)
                .limit(6)
                .collect(Collectors.toList());

        List<Integer> expected = Stream.generate(() -> items)
                .flatMap(List::stream)
                .limit(6)
                .collect(Collectors.toList());
        assertEquals(List.of(3, 2, 1, 3, 2, 1), result);
    }

    @Test
    void resetReturnsToStart() {
        list.add(1);
        list.add(2);
        list.add(3);

        list.next();
        list.next();

        list.reset();

        assertEquals(1, list.next().get());
    }

    @Test
    void nextWithStrategyReturnsFirstMatch() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        assertEquals(2, list.next(x -> x % 2 == 0).get());
    }

    @Test
    void nextWithStrategyReturnsEmptyIfListIsEmpty() {
        assertFalse(list.next(x -> x % 2 == 0).isPresent());
    }

    @Test
    void nextWithStrategyReturnsEmptyIfNoItemsMatch() {
        list.add(1);
        list.add(3);
        list.add(5);

        assertFalse(list.next(x -> x % 2 == 0).isPresent());
    }

    @Test
    void nextWithStrategyDoesntMoveTheCursorIfNoItemsMatch() {
        list.add(1);
        list.add(3);
        list.add(5);

        list.next(x -> x % 2 == 0);

        assertEquals(1, list.next().get());
    }
}
